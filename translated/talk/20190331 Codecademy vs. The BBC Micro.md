[#]: subject: "Codecademy vs. The BBC Micro"
[#]: via: "https://twobithistory.org/2019/03/31/bbc-micro.html"
[#]: author: "Two-Bit History https://twobithistory.org"
[#]: collector: "lujun9972"
[#]: translator: "CanYellow"
[#]: reviewer: " "
[#]: publisher: " "
[#]: url: " "

[BBC Micro][T2] 比之 [Codecademy][T1]
======

20世纪70年代末期，计算机突然成为了某种普罗大众能够买回家的商品；而此前的几十年间，它一直只是听命于企业霸主的神秘而笨重的机器。少数狂热的爱好者注意到了这是多么吸引人并争相购买了属于自己的计算机。对更多的人而言，微形计算机的到来引发了对未来的无助焦虑。同时期的杂志上的一则广告承诺一台家用计算机将“让您的孩子在学校享有不公平的优势”。广告中展示了一位打着领带，身着时髦的西装外套的男孩子急切地举手回答问题，在他身后，他的显得不那么聪明的同学们闷闷不乐地望着他。这则广告以及其它与之类似的广告表明世界正在疾速改变，而如果你不立即学习如何使用这些令人生畏的新设备之一，你和你的家人就会被时代所抛弃。

在英国，这些焦虑转化为政府高层对国家竞争力的担忧。从各种意义上，20世纪70年代对英国来说都是平平无奇的十年，通胀与失业率高企。与此同时，一系列的罢工让伦敦陷于一次又一次的停电中。一篇1979年的政府报告焦虑：没有跟上计算机技术浪潮将“为我们糟糕的工业表现平添又一个影响因素”[1][1]。英国似乎已经在计算机技术的角逐中落后了——所有的大型的计算机公司都是美国的，而集成电路则在日本和中国台湾制造。

BBC（英国广播公司）——由政府建立的公共服务广播公司——作出了一个大胆的举动，决定通过帮助英国人战胜他们对计算机的反感来解决英国的国家竞争力问题。BBC 发起了 [_Computer Literacy Project(计算机认知计划)_][T3]，该计划包括多个教育方向的努力：几部电视系列片，一些相关书籍，一张支持团队网络以及一款名为 BBC Micro 的特别定制的微型计算机。该项目是如此成功以致于到1983年[ BYTE 杂志][T4]的一名编辑写道：“与美国相比，有更多的英国人对微型计算机感兴趣。”[2][2] 这名编辑惊讶于英国的 Fifth Personal Computer World Show(第五届个人电脑世界展) 比当年的西海岸电脑展的人数更多。超过六分之一的英国人观看了由 _Computer Literacy Project_ 制作的第一部电视系列片的一集并最终售出了150万台 BBC Micro 微型计算机。[3][3]

去年，一份包括了由 _Computer Literacy Project_ 出版的所有材料与制作的每一部电视系列片的[档案][4]发布在了互联网上。我抱着极大的兴趣观看这些电视系列片并试图想象在20世纪80年代早期学习电脑使用是什么图景。不过我发现更有兴趣的是时年是如何教授电脑使用的。今天，我们仍然担心技术发展使人们落伍。富有的科技企业家与政府花费大量的资金试图教孩子们“编程”。我们拥有诸如 Codecademy 这样的通过新技术的运用进行交互式编程教学的网站。我们可能假定这种方式比80年代的呆板的电视系列片更高效，不过真的是这样吗？

### [Computer Literacy Project][T3] (计算机认知计划)

微型计算机革命始于1975年 [Altair 8800][5] 的发布。两年后，Apple II，TRS-80 以及 Commodore PET 都相继发布。全新的计算机的销量爆发式增长。1978年，BBC 在一部名为["Now the Chips Are Down"][T5]（《芯片来了》）（译者注：对于非英国区域的读者，可以在[这里][T6]观看该纪录片） 的纪录片中探讨了这种新的机器必将带来的剧烈的社会变革。

该纪录片充满担忧。在前5分钟内，解说员提到这种微电子器件将“彻底改变我们的生活方式”。随着诡异的合成音乐的播放以及屏幕上绿色的电脉冲在放大后的芯片上起舞，解说员进一步说这种芯片“正是日本放弃造船业的原因，也将成为我们的孩子们长大后失业的原因”。纪录片继续探讨了机器人如何用于汽车自动化组装以及欧洲的手表工业如何在与美国的电子表工业竞争中败下阵来。它斥责了英国政府在应对未来的大规模失业的准备上做得不够。

该纪录片据信可能在英国议会上展示过。[4][6] 包括工业署和人力服务委员会在内的一些政府代表开始有兴趣尝试提高英国公众对计算机的认识。人力服务委员会为来自 BBC 的教育部门的一个团队到日本、美国以及其他国家的实地考察提供了资助。该研究团队完成了一份历数微电子技术在工业制造、人力关系与办公室工作等领域最终将意味着哪些方面的重大改变的研究报告。70年代末，BBC 决定制作一部帮助普通英国人“学习如何使用和控制计算机，避免产生被计算机支配的感受”的十集电视系列片[5][7] 。这一努力最终成为了一个与_Adult Literacy Project_（成人扫盲计划）相似的多媒体项目。_Adult Literacy Project_是 BBC 此前进行的一项涉及电视系列片以及辅助课程的帮助两百万人提高他们的阅读能力的项目。

_Computer Literacy Project_ 背后的制作方热衷于以“实操”示例为特色的电视系列片。这样如果观众拥有一台微型计算机在家里，他们就可以亲自动手尝试。这些例子不得不都是基于 BASIC 语言的，因为这是在几乎所有的微型计算机上都使用的编程语言 (实际是整个交互界面(shell)）。但是制作方面临一个棘手的问题：微型计算机制造商均拥有他们自己的 BASIC 方言，因此不论他们选择哪一种方言，他们都不可避免地疏远大部分的观众。唯一切实可行的方案是创造一种全新的 BASIC 方言——BBC BASIC——以及与之配合使用的微型计算机。英国公众就可以购买这种全新的微型计算机并依照示例操作而不需要担心软硬件上的差异带来的问题。

BBC 的制作人与节目主持人并不具备自行制造微型计算机的能力，因此他们汇总了一份他们预期的计算机的规范并邀请英国的微型计算机公司推出满足该规范要求的新机器。这份规范要求一种相对更强劲的计算机，因为 BBC 的制作方认为相应的设备应当能够运行真实有用的应用程序。_Computer Literacy Project_的技术顾问也是如此建议：如果必须要教授全体国人一种 BASIC 方言的话，那么最好选择表现良好的(他们可能没有完全那样说，不过我认为这就是他们的真实想法)。BBS BASIC 通过允许递归调用与局部变量弥补了一些 BASIC 语言的常见缺点。[6][8]

BBC 最终决定由坐落于剑桥的 Acorn Computers 公司(中文网络译名：艾康电脑)制造 BBC Micro 计算机。在选择 Acorn 公司的时候，BBC 并未考虑来自经营 Sinclair Research 公司的 [Clive Sinclair][T7] 的申请，Sinclair Research 公司凭借 Sinclair ZX80 微型计算机的推出在1980年将微型计算机的大众市场引入了英国。Sinclair 公司的新产品，ZX81，虽然更便宜但是性能不足以满足 BBC 的要求。Acorn 的新型的内部称为 Proton 原型计算机更加昂贵但是性能更好，更具备扩展性，BBC 对此印象深刻。该型号的计算机从未作为 Proton 进行营销与销售，Acorn 公司代之以 BBC Micro 的名称在1981年12月发布。BBC Micro 又被亲切地称为“The Beeb”，你可以以235英磅的价格购得其16k内存的版本或者以335英磅的价格获得其32k内存的版本。

1980年，Acorn 是英国计算机工业的失败者，但是 BBC Micro 成就了 Acorn 公司的传统。时至今日，世界范围内最流行的微处理器指令集是 ARM 架构，“ARM”如今表示“Advanced RISC Machine”(先进 RISC 架构设备)，然而最初它代表的是“Acorn RISC Machine”(Acorn RISC 架构设备)。ARM 架构背后的 ARM Holding(ARM 公司) 就是 Acorn 公司在1990年之后的延续。

![Picture of the BBC Micro.][9] _BBC Micro 的一幅拙劣图片，我摄于美国加州山景城(Mountain View)的计算机历史博物馆(Computer History Museum)_


### 名为“The Computer Programme”(计算机程序)的电视系列片

十几个不同的电视系列片最终作为_Computer Literacy Project_的一部分创作出来。第一部作品是一部名为 _The Computer Programme_(计算机程序) 的十集电视系列片。该系列片在1982年初播出了十周。一百万人每周晚上收看该节目，另有25万人收看该节目在每周日与周一下午的重播。

这一电视节目有两名主持人，Chris Serle 和 Ian McNaught-Davis。Serle 扮演初学者，而 McNaught-Davis 扮演具有大型计算机编程职业经验的专家，这是一个启发性的配置。该节目造就了[略显尴尬的过渡][10]——Serle 经常直接从与 McNaught-Davis 的对话中过渡到面向镜头的边走边说的讲述，此时你不禁会疑惑 McNaught-Davis 是否还站在画面之外。不过这意味着 Serle 可以表达观众肯定会有的关注。他可能会惊恐地看着满屏的 BASIC 语言并提出类似“这些美元符号是什么意思”的问题。在节目中的某些时刻，Serle 与 McNaught-Davis 会坐在电脑前进行事实上的结对编程。McNaught-Davis 会在不同的地方留下一些线索，而 Serle 则试图将它们弄清楚。如果这一节目仅仅由一个无所不知的讲述者主持，它将更难以理解。

该节目也在努力展示计算在普通人生活中的实际应用。到80年代早期，家庭电脑已经开始与小孩子和电子游戏联系在一起。_Computer Literacy Project_ 的制作方试图避免采访“令人印象深刻的有能力的年轻人”，因为这可能会“加剧老年观众的焦虑”，而该节目正是试图吸引这一人群对计算感兴趣[7][11]。在该系列的第一集中，该节目的“现场”记者 Gill Nevill 采访了一名购买了一台 Commodore PET 电脑用于辅助管理她的糖果店的女性。这名名叫 Phyllis 的女性受访者看上去大约60多岁，她在使用 PET 完成她的会计工作上不存在任何问题，甚至开始使用 PET 为其他企业做计算工作，这听上去像是一个有前途的自由职业的开端。Phyllis 说她并不介意电脑工作逐步取代她的糖果店生意，因为她更喜欢电脑工作。画面接着变成了对一名青少年的采访，他介绍了他是如何修改 _[Breakout][T8]_ 这款电子游戏以使之运行更快并更具挑战性，不过这几乎鼓舞不了任何人。另一方面，如果人群中的 Phyllis 也会使用电脑，那么你当然也可以。

虽然该节目的特色是大量的 BASIC 编程，不过它实际想要教给观众的是计算机通常是如何工作的。该节目通过类比的方法解释了其中的一般原则。在第二集中有一个关于[Jacquard][T9]织机(译注：中文网络译为雅卡尔提布机)的讨论。Jacquard 织机实现了两件事。其一，它揭示了计算机并不仅仅基于过去发明的神秘技术——计算的一些基本原则可以上溯到两百年前，就跟你可以在纸上打孔来控制纺织机的想法一样简单。其二，经线与纬线的交织用于解释选择二进制（即纬线是从上方还是下方穿过经线）是如何在重复了一次又一次之后足以产生巨大变化的。当然，节目接下来继续讨论信息是如何使用二进制存储的。

在该节目中接下来是有关一件能够播放在一卷长长的分段的打孔卡片上编码的音乐的蒸汽风琴的小节。这个类比用以解释 BASIC 中的子程序。Serle 与 McNaught-Davis 将整卷的打孔卡片摊开在工作室的地板上，然后指出看上去像是重复的副歌的分段。McNaught-Davis 解释说，如果你将这些重复的卡片分段剪下而插入一个回到第一次播放副歌的分段的指令，这就是子程序。这是一个绝妙的解释并将可能在听众的脑海中久久挥之不去。

我仅仅摘录了一些例子，不过我认为总的来看该节目尤为擅长通过解释计算机实现功能所依赖的原则使计算机不再神秘。这一节目本可以致力于 BASIC 教学，不过它并没有这样做。这被证明是一个相当明智的选择。在1983年写就的一篇回忆文章中，_Computer Literacy Project_的总制作人 John Radcliffe 如是写道：

> 如果计算机将如我们所相信的那样重要，对这一主题的真正理解对每个人都很重要，可能会几乎与文字读写能力同等重要。不管是在我们这里还是在美国，在计算机认知的主要路线上的早期思路均集中于编程上。然而随着我们思想的发展，我们意识到“亲自”体验在个人计算机上的价值，我们开始降低对编程的重视，而更多的强调广泛的理解，将微型计算机与大型计算机联系起来，鼓励人们获取一系列应用程序与高级语言的经验，并将这些经验同现实世界中的工业与商业活动中的经验联系起来…… 我们相信一旦人们掌握了这些原则，简单地说，它们将可以进一步深入该主题。

接下来， Radcliffe writes 又以类似的方式写道：

> 围绕着这一电视系列片的主要阐释目标有很多的争论。一个思想流派认为在使用微型计算机上的实用细节上给予建议对本项目而言尤为重要。但我们已经有了结论，如果这一电视系列片能够拥有可持续性的教育价值，它必须成为一条通过解释计算原则来进入真实的计算世界的路径。这必须通过对微型计算机的室内演示，通过类比方式解释其中的原则，真实生活中的实际应用的影片展示这三者的结合实现。不仅仅是微型计算机，小型机以及大型机也将被展示。

我喜爱这一系列片，尤其是其中关于小型机与大型机的部分。_Computer Literacy Project_ 背后的制作方旨在帮助英国找准定位：计算身处何处又去向何方？计算机现在能做什么，未来又能做什么？学习一些 BASIC 语言是回答这些问题的一个部分，但是仅仅理解 BASIC 语言似乎是不足以使人们认知计算机的。

### 今天的计算机认知

如果你现在搜索“学习编程”，你看到的排在第一的是指向 Codecademy 网站的链接。如果要说存在一个“计算机认知计划”的现代替代品——存在相同的影响与目标，那就是 Codecademy。

对于普通人而言“Learn to code”（学习编程）是 Codecademy 的口号。我认为我不是第一个指出这一点的人——事实上我曾经在某个地方见到过这一点，只是现在拿来用而已。但是这里使用的是“code”（代码）而非“编程”，这说明了一些问题。这表明你学习的重要内容是如何读懂代码，如何阅读满屏的 Python 代码的价值而不是目光呆滞、不知所云。我能够理解为什么对于普通人而言这似乎是成为专业程序员的主要障碍。专业程序员整日盯着布满编程术语的电脑屏幕，如果我想要成为一个专业程序员，我最好确保我能够理解这些术语。但是理解语法并不是成为程序员的最大的挑战。面对更多更大的障碍，它很快将变成微不足道的。仅仅以掌握一门编程语言的语法为目标，你可能能够 _阅读_ 代码但是无法做到 _编写_ 代码解决全新的问题。

我最近浏览了 Codecademy 的 “Code Foundations”(编程基础) 课程。如果你对编程感兴趣(而不是网页开发或者数据科学)并且没有任何编程经验，这是 Codecademy 推荐你学习的课程。里面有几节关于计算机科学史的课时，不过都是流于表面而没有深入研究。（感谢上帝，一位高尚的互联网秩序义务维护者指出了其中存在的一个特别恶劣的错误）。该课程的主要目的是教授你编程语言的通用结构要素：变量、函数、控制流、循环等。换句话说，该课程聚焦于为了开始理解编程术语中的模式你所需要知道的内容。

公平地看，Codecademy 也提供其他内容深入的课程。但是即使是如 “Computer Science Path”(计算机科学之路) 这样的课程也几乎只仅仅专注于编程以及程序中表达的概念。有人可能会反驳说这就是重点—— Codecademy 的主要特点就是提供给你一些交互式的带有自动反馈的编程课程。在有限的自动化课程中能够灌输给学员的内容只有这么多，因此学员的脑海里也没有更多的空间容纳更多其他的内容。但是负责启动 _Computer Literacy Project_ 的 BBC 的制作人也面临同样的问题。他们意识到受限于他们的传播媒介，“通过电视节目所能获得的学习内容的容量也是受限的”[8][13]。虽然在他们所能传达的信息总量上存在相似的限制，但是 BBC 的制作人选择强调在学习 BASIC 语言上的一般原则。Codecademy 就不能将其中一两节交互式可视化的课时替换为编织经线与纬线的 Jacquard 织机的案例吗？

我一直在大声鼓吹“一般原则”，因此让我再解释下我认为的一般原则是什么以及为什么它们如此重要。J. Clark Scott 出了一本有关计算机的书，书名为 _But How Do It Know?_（但是它怎么知道）。这个书名来自书的序言里的一则笑话：一个店员向人群推销保温瓶，说保温瓶可以让热食始终是热的，冷食始终是冷的。一名听众对这个新发明感到惊讶，问道，但是它怎么知道（根据你给它的食物类型的不同选择做相应的事情呢）？笑点在于保温瓶当然不能感知食物的温度然后据此做出决定——保温瓶仅仅制作成保证冷食必然保持冷的，热食必然保持热的就可以了。人们也以(笑话中的那个听众)一样的方式看待计算机，相信计算机就是能够基于提供给它们的代码“选择”做一件事或者另一件事的数字大脑。但是了解一些有关计算机如何工作的知识，哪怕是很初级水平的理解，也能让（人们理解中的）计算机摆脱（做判断的）侏儒。这就是为什么 Jacquard 织机是一个很好的有助理解的例子。一开始它似乎是一种难以置信的设备，它读取打孔卡片，然后以某种方式“知道”编织正确的样式。现实是显而易见的：每一行孔都对应一根线，而一行中有孔的地方对应着提起的线。理解了这些虽然不会有助于你用电脑完成新的事情，但是将使你自信于你不是在跟某些神秘事物打交道。我们应当尽快将这种自信的感受传授给初学者。

唉，真实存在的问题是可能没有人想要了解 Jacquard 织机。根据 Codecademy 如何强调他们教授的专业应用来判断，很多人开始使用 Codecademy 可能是因为他们相信这有助于“提升”他们的职业生涯。他们没有来由地相信首要的问题是理解编程的专业术语，因此他们才想要 “Learn to code”。他们想要在他们所拥用的每天晚上晚餐与就寝之间的一两个小时里尽快做这件事。Codecademy 毕竟只是一门投其所好的生意而非一些有关18世纪就发明了的机器的间接说明。

另一方面，_Computer Literacy Project_ 是供职于 BBC 的一群制作人与公务员所认为的将电脑使用教给国民的最好的方式。我承认因为这一群人教会大众他们无法以己之力所能求得的事物而赞美这一群人的建议多少有点精英主义。但我情不自禁认为他们做对了。许多人使用 BBC Micro 第一次学会了使用电脑，他们中的很多人进而成为了成功的软件开发者或游戏设计师。[正如我曾经所言][14]，我怀疑在计算机已经变得相对简单的时代里学习使用电脑是一个巨大的优势。不过或许这群人所拥有的另一个优势在于有像 _The Computer Programme_ 这样的尽己所能不仅仅教授编程而且教授计算机是为什么又是如何运行程序的节目。在看完  _The Computer Programme_ 之后，你可能并不能理解电脑屏幕上的所有编程术语，但是实际上你并不需要，因为你知道无论“代码”是什么样子，计算机总是在重复做基础的事情。在完成了 Codecademy 上的一到两个课程之后，你可能理解了编程术语的一些味道，但是对你来说一台电脑仍然只是一台能够以某种方式将编程术语转化为运行的软件的魔法机器。这并不是计算机认知。


  1. Robert Albury and David Allen, Microelectronics, report (1979). [↩︎][18]

  2. Gregg Williams, “Microcomputing, British Style”, Byte Magazine, 40, January 1983, accessed on March 31, 2019, <https://archive.org/stream/byte-magazine-1983-01/1983_01_BYTE_08-01_Looking_Ahead#page/n41/mode/2up>. [↩︎][19]

  3. John Radcliffe, “Toward Computer Literacy,” Computer Literacy Project Achive, 42, accessed March 31, 2019, [https://computer-literacy-project.pilots.bbcconnectedstudio.co.uk/media/Towards Computer Literacy.pdf][20]. [↩︎][21]

  4. David Allen, “About the Computer Literacy Project,” Computer Literacy Project Archive, accessed March 31, 2019, <https://computer-literacy-project.pilots.bbcconnectedstudio.co.uk/history>. [↩︎][22]

  5. ibid. [↩︎][23]

  6. Williams, 51. [↩︎][24]

  7. Radcliffe, 11. [↩︎][25]

  8. Radcliffe, 5. [↩︎][26]




--------------------------------------------------------------------------------

via: https://twobithistory.org/2019/03/31/bbc-micro.html

作者：[Two-Bit History][a]
选题：[lujun9972][b]
译者：[CanYellow](https://github.com/CanYellow)
校对：[校对者ID](https://github.com/校对者ID)

本文由 [LCTT](https://github.com/LCTT/TranslateProject) 原创编译，[Linux中国](https://linux.cn/) 荣誉推出

[a]: https://twobithistory.org
[b]: https://github.com/lujun9972
[1]: tmp.zNBs2lK4Ca#fn:1
[2]: tmp.zNBs2lK4Ca#fn:2
[3]: tmp.zNBs2lK4Ca#fn:3
[4]: https://computer-literacy-project.pilots.bbcconnectedstudio.co.uk/
[5]: https://twobithistory.org/2018/07/22/dawn-of-the-microcomputer.html
[6]: tmp.zNBs2lK4Ca#fn:4
[7]: tmp.zNBs2lK4Ca#fn:5
[8]: tmp.zNBs2lK4Ca#fn:6
[9]: https://twobithistory.org/images/beeb.jpg
[10]: https://twitter.com/TwoBitHistory/status/1112372000742404098
[11]: tmp.zNBs2lK4Ca#fn:7
[12]: https://twitter.com/TwoBitHistory/status/1111305774939234304
[13]: tmp.zNBs2lK4Ca#fn:8
[14]: https://twobithistory.org/2018/09/02/learning-basic.html
[15]: https://twitter.com/TwoBitHistory
[16]: https://twobithistory.org/feed.xml
[17]: https://twitter.com/TwoBitHistory/status/1091148050221944832?ref_src=twsrc%5Etfw
[18]: tmp.zNBs2lK4Ca#fnref:1
[19]: tmp.zNBs2lK4Ca#fnref:2
[20]: https://computer-literacy-project.pilots.bbcconnectedstudio.co.uk/media/Towards%20Computer%20Literacy.pdf
[21]: tmp.zNBs2lK4Ca#fnref:3
[22]: tmp.zNBs2lK4Ca#fnref:4
[23]: tmp.zNBs2lK4Ca#fnref:5
[24]: tmp.zNBs2lK4Ca#fnref:6
[25]: tmp.zNBs2lK4Ca#fnref:7
[26]: tmp.zNBs2lK4Ca#fnref:8

[T1]: https://www.codecademy.com/
[T2]: https://bbcmicro.computer/
[T3]: https://clp.bbcrewind.co.uk/history
[T4]: https://archive.org/details/byte-magazine?tab=about
[T5]: https://www.bbc.co.uk/iplayer/episode/p01z4rrj/horizon-19771978-now-the-chips-are-down
[T6]: https://archive.org/details/BBCHorizon19771978NowTheChipsAreDown
[T7]: https://en.wikipedia.org/wiki/Sinclair_Research
[T8]: https://en.wikipedia.org/wiki/Breakout_(video_game)
[T9]: https://www.scienceandindustrymuseum.org.uk/objects-and-stories/jacquard-loom
